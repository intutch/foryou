module.exports = function (pool, todoRepo) {
    return {
        async list(ctx) {
            ctx.body = await todoRepo.list(pool)
        },
        async create(ctx) {
            const todo = ctx.request.body
            // TODO: validate todo
            console.log(todo)
            const id = await todoRepo.create(pool, todo['text'])
            ctx.body = {
                todo
            }
        },
        async get(ctx) {
            const id = ctx.params.id
            let getTodo = await todoRepo.find(pool, id)
            ctx.body = getTodo
            // TODO: validate id
            // find todo from repo
            // send todo
        },
        async update(ctx) {
            const id = ctx.params.id
            const content = ctx.request.body
            let doPatch = await todoRepo.changeContent(pool, id, content['text'])
            let getTodo = await todoRepo.find(pool, id)
            ctx.body = getTodo
        },
        async removeTodo(ctx) {
            const id = ctx.params.id
            let removeTodo = await todoRepo.remove(pool, id)
            ctx.body = 'OK remove'
        },
        async complete(ctx) {
            const id = ctx.params.id
            await todoRepo.markComplete(pool, id)
            let getTodo = await todoRepo.find(pool, id)
            ctx.body = getTodo
        },
        async incomplete(ctx) {
            const id = ctx.params.id
            await todoRepo.markIncomplete(pool, id)
            let getTodo = await todoRepo.find(pool, id)
            ctx.body = getTodo
        },
    }
}