module.exports = {
  create,
  list,
  find,
  changeContent,
  remove,
  markComplete,
  markIncomplete
}

async function create(db, todo) {
  let result = await db.execute(`insert into todo (text) values (?)`, [todo])
  return [result]
}

function createEntity(row) {
  return {
    id: row.id,
    text: row.text,
    status: row.status
  }
}
async function list(db) {
  let [rows] = await db.execute(`select text from todo`)
  return rows.map(createEntity)
}

async function find(db, id) {
  let [rows] = await db.execute(`select id,text,status from todo where id = ?`, [id])
  return createEntity(rows[0])
}

function changeContent(db, id, content) {
  return db.execute(`update todo set text = ? where id = ?`, [content, id])
}

async function remove(db, id) {
  return db.execute(`delete from todo where id = ?`, [id])
}

function markComplete(db, id) {
  return db.execute(`update todo set status = 1 where id = ?`, [id])
}

async function markIncomplete(db, id) {
  await db.execute(`update todo set status = 0 where id = ?`, [id])
}